fn main(){
    let mut counter = 0;
    for i in 0..10 {
        counter = incr(counter);
        println!("roop={},counter={}",i,counter);
    }
}

fn incr(cnt: i32) -> i32{
    if cnt >= 8 {
        return 1;
    }
    else{
        cnt + 1
    }
}
