

struct Point{
    x: i32,
    y: i32,
}

struct Cube{
    width: i32,
    height: i32,
    depth: i32,
}

impl Cube{
    fn new(width: i32, height: i32, depth: i32) ->Cube{
        Cube{width: width, height: height,depth: depth}
    }
    
    fn get_num_surface() -> i32 {
        6
    }

    fn get_volume(&self) -> i32{
        self.width * self.height * self.depth
    }

    fn get_area(&self) -> i32{
        self.width * self.height * 2 + self.height * self.depth * 2 + self.depth * self.width * 2
    }

}



static GLOBAL: i32 = 99;
fn main(){
    rawptr2();
    mutrowptr();
    boxx();
    str1();
    array1();
    array3();
    strcode();
    array4();
    array5();
    structx();
    implx();
    staticmethod();
    vec1();
    string1();
    string2();
    slice1();
    slice2();
    slice3();
}


fn slice3(){
    println!("[slice3]");
    let ohm = String::from("小宮賢士");
    let slice = &ohm[..6];

    println!("slice = {:?}",slice);
}

fn slice2(){
    println!("[slice2]");
    let msg = String::from("My name is Alice");
    let slice1 = &msg[..7];
    let slice2 = &msg[11..msg.len()];

    println!("slice1 = {:?}",slice1);
    println!("slice2 = {:?}",slice2);

}

fn slice1(){
    println!("[slice1]");
    let array = [0,1,2,3,4,5,6,7,8,9];
    let first:&[i32]  = &array[0..5];
    let second = &array[5..10];

    println!("first = {:?}",first);
    println!("second = {:?}",second);
}

fn string2(){
    println!("[string2]");
    let mut msg = String::new();
    msg.push_str("Hello");
    msg.push(' ');
    msg.push_str("world.");

    println!("masg = {}",msg);
}


fn string1(){
    println!("[string1]");
    let msg: String = String::from("Hello world.");
    let name = "My name is Alice.".to_string();
    println!("msg = {}",msg);
    println!("name = {}",name);
}

fn vec1(){
    println!("[vec1]");
    let mut vec: Vec<i32> = Vec::new();
    vec.push(1);
    vec.push(14);
    vec.push(5);

    println!("len = {}",vec.len());
    println!("element = {:?}",vec);
    println!("vec[1] = {:?}",vec.get(1));
    println!("vec[1] = {}",vec.get(1).unwrap());

    vec.remove(1);
    println!("removed vec = {:?}",vec);

}


fn staticmethod(){
    println!("[staticmethod]");
    let cube = Cube::new(10,20,50);
    println!("length = ({},{},{})", cube.width, cube.height, cube.depth);
    println!("surface num = {}",Cube::get_num_surface());
}

fn implx(){
    let cube = Cube{width: 10,height: 20,depth: 30};
    println!("length = ({},{},{})",cube.width,cube.height,cube.depth);
    println!("volume = {}",cube.get_volume());
    println!("volume = {}",cube.get_area());

}

fn structx(){
    let p = Point{x:10,y:20};
    println!("(x,y)=({},{})",p.x,p.y);
}

fn array5(){
    let name: [&str;4] = ["Alice","Bob","Chris","David"];

    for i in 0..4{
        println!("names[{}] = {}", i,name[i].len());
    }
}

fn array4(){
    const N: usize = 3;
    const M: usize = 5;
    let mut array = [[0;M]; N];

    for i in 0..N{
        for j in 0..M{
            array[i][j] = 10 * i + j +1;
        }
    }

    for i in 0..N{
        for j in 0..M{
            println!("array[{}][{}] = {}",i,j,array[i][j]);
        }
    }
}

fn strcode(){
    let letters = "abcde";
    println!("len = {}",letters.len());
    println!("UTF8={:?}",letters.as_bytes());

    let name = "小宮賢士";
    println!("len = {}",name.len());
    println!("UTF8 = {:?}",letters.as_bytes());

}



fn array3(){
    println!("[array3]");
    const N: usize = 10;
    let mut array = [0;N];

    for i in 0..N{
        array[i] = 10 * i;
    }
    for i in 0..N{
        println!("array[{}] = {}",i,array[i]);
    }
}

fn array1(){
    println!("[array1]");
    let array = [2,4,6,8,10];
    for i in 0..5{
    println!("[str]");
        println!("array[{}] = {}",i,array[i]);
    }
}

fn str1(){
    println!("[str]");

    let name: &str = "Alice";
    println!("name = {}",name);
    println!("name.len = {}",name.len());
    println!("name.upper = {}",name.to_uppercase());
}

fn boxx(){
    println!("[box]");
    let b: Box<i32> = Box::new(10);
    println!("b = {}",b);
}

fn mutrowptr(){
    println!("[mutrowptr]");
    let local: i32 = 10;
    let mut ptr: *const i32 = &local;
    
    unsafe{
        println!("*ptr = {}", *ptr);
    }
    println!("ptr = {:?}",ptr);

    ptr = &GLOBAL;
    unsafe{
        println!("*ptr = {}", *ptr);
    }
    println!("ptr = {:?}",ptr);
}

        
    

fn rawptr2(){
    println!("[rawptr2]");
    let mut x: i32 = 1;
    let ptr: *mut i32 = &mut x;

    unsafe{
        println!("pointer = {}",*ptr);
        println!("ptr = {:?},x={}",ptr,x);
        
        *ptr = 99;
        println!("ptr = {:?},x={}",ptr,x);

    }
}


