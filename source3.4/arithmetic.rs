fn main() {
    let x = 10;
    let y = 20;
    let mut z;

    z = x + y;
    println!("{} + {} = {}",x,y,z);

    z = x - y;
    println!("{} - {} = {}",x,y,z);

    z = x * y;
    println!("{} * {} = {}",x,y,z);
}

